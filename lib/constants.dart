import 'package:flutter/material.dart';

const kPrimaryColor = Color(0xFFF62626);
const kPrimaryLightColor = Color(0xFFE1FAF8);

const double defaultPadding = 16.0;
